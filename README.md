## Setup
### Decrypt vulnerable files
```sh
git-crypt unlock PATH_TO_KEY
```

### Get Phoenix dependencies
```sh
cd pop_umbrella
mix deps.get
```

### Set up database
```sh
cd apps/pop
mix ecto.reset
```

### Compile Elm
```sh
cd ../pop_web/assets
yarn
gulp --env production
```

### Compile client frontend
```sh
cd ../../../../frontend/client
yarn
gulp --env production
```

### Compile admin frontend
```sh
cd ../admin
yarn
yarn run build dev|review|prod
```

## Run
### Run Elm
```sh
cd pop_umbrella/apps/pop_web/assets
gulp
```

### Run Phoenix
```sh
cd pop_umbrella
./start HOST PORT
```

### Run Phoenix with e
#### Initialize e project
```sh
cd pop_umbrella
e init pop -t e-template.exs
```
#### Run e project
```sh
e run pop
```

#### Credentials
To log-in into the admin interface in dev environment, use the credentials below:

username: `admin@test.entropia.hr`
password: `testtest`
