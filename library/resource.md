## l10n.contents embeds
```json
{
  type: "resources",
  ids: [<integer>]
}
|
{
  type: "cta_petition" | "cta_event",
  id: <integer>,
  title: <string>
}
|
{
  type: "cta_mass_email",
  id: <integer>,
  title: <string>,
  content: <string>
}
|
{
  type: "cta_tweet",
  id: <integer>,
  title: <string>,
  text: <string>,
  content: <string>,
  hashtags: [<string>]
}
|
{
  type: "cta_retweet",
  id: <integer>,
  title: <string>,
  text: <string>,
  tweet_id: <string>
}
|
{
  type: "cta_act",
  id: <integer>,
  title: <string>,
  text: <string>,
  hashtag: <string>,
  file: <string>,
  file_name: <string>,
  file_size: <string>,
  thumbnail_url: <string>
}
|
{
  type: "cta_external_act",
  id: <integer>,
  title: <string>,
  text: <string>,
  external_url: <string>,
  external_url_caption: <string>,
  thumbnail_url: <string>
}
|
{
  type: "gallery",
  ids: [<integer>]
}
|
{
  type: "video",
  id: <integer>
}
|
{
  type: "text",
  text: <string>
}
|
{
  type: "quote",
  text: <string>,
  source: <string>,
  thumbnail_url: <string>
}
|
{
  type: "share"
}
|
{
  type: "endorsements"
}
|
{
  type: "newsletters"
}
|
{
  type: "tweets",
  hashtags: [<string>]
}
|
{
  type: "gdoa_events"
}
|
{
  type: "map",
  ids: [<integer>]
}
```
## l10n
#### "archive_mm" | "audio" | "image" | "video" | "archive_doc" | "doc" | "pdf" | "ppt" | "xls" | "article"
```json
{
  lang: <string>,
  title: <string>,
  foreword: <string>,
  description: <string>,
  status: "published" | "draft" | "disabled"
}
```
#### "campaign" | "campaign_blog" | "case_study" | "project" | "gdoa" | "project | "project""
```json
{
  lang: <string>,
  title: <string>,
  foreword: <string>,
  description: <string>,
  contents: [<embed>],
  status: "published" | "draft" | "disabled"
}
```
#### "event" | "gdoa_event"
```json
{
  lang: <string>,
  title: <string>,
  foreword: <string>,
  description: <string>,
  location_info: <string>,
  contents: [<embed>],
  status: "published" | "draft" | "disabled"
}
```
#### "external_event" | "gdoa_external_event"
```json
{
  lang: <string>,
  title: <string>,
  foreword: <string>,
  description: <string>,
  location_info: <string>,
  status: "published" | "draft" | "disabled"
}
```

## Contents
#### "archive_mm" | "audio" | "image" | "video" | "article"
```json
{
  external_url: <string>,
  thumbnail_url: <string>,
  source_name: <string>
}
```
#### "archive_doc" | "doc" | "pdf" | "ppt" | "xls"
```json
{
  thumbnail_url: <string>,
  external_url: <string>
}
```
#### "campaign" | "case_study" | "project | "project""
```json
{
  thumbnail_url: <string>,
  location:
  {
    latitude: <float>,
    longitude: <float>
  }
}
```
#### "campaign_blog"
```json
{
  campaign_id: <integer>
}
```
#### "gdoa"
```json
{
  thumbnail_url: <string>,
  start: <string>,
  end: <string>
}
```
#### "event"
```json
{
  thumbnail_url: <string>,
  location:
  {
    latitude: <float>,
    longitude: <float>
  }
}
```
#### "gdoa_event"
```json
{
  gdoa_id: <integer>,
  thumbnail_url: <string>,
  location:
  {
    latitude: <float>,
    longitude: <float>
  }
}
```
#### "external_event"
```json
{
  thumbnail_url: <string>,
  external_url: <string>,
  source_name: <string>,
  location:
  {
    latitude: <float>,
    longitude: <float>
  }
}
```
```
#### "gdoa_external_event"
```json
{
  gdoa_id: <integer>,
  thumbnail_url: <string>,
  external_url: <string>,
  source_name: <string>,
  location:
  {
    latitude: <float>,
    longitude: <float>
  }
}
```

## Get resource
#### Request
```json
{
  path: "/api/resource/<resource_id>",
  method: "GET",
  body: {
    api_keys: [<string>],
    password: <string>
  }
}
```
#### Notes
-  api_keys are required.
- password is required if resource is password protected
- Admins and org admins dosnt need to enter password, org members and guest have to
- Expands contents' embeds by adding fetched resources (fetched by `embed.id` or `embed.ids`) under a new key named the same as embed's type (e.g. "resources" are added under "resources").
- Adds all associated `report`s into `contents`.
- Security level: none. Resources (embeds as well) with `status` "draft" or "disabled" are only visible to **admin**. `contents.reports` are only available to **admin**.
- Adds `org` field with organisation fields. (See `accounts/org.md`)

## Create resource
#### Request
```json
{
  path: "/api/resource/create",
  method: "POST",
  body:
  {
    type: "archive_mm" | "audio" | "image" | "video" | "archive_doc" | "doc" | "pdf" | "ppt" | "xls" | "article" | "campaign" | "campaign_blog" | "case_study" | "gdoa" | "event" | "gdoa_event" | "external_event" | "gdoa_external_event" | "project",
    password: <string>,
    tag_ids: [<integer>],
    l10n: [<l10n>],
    contents: <contents>,
    is_featured: <boolean>,
    status: "published" | "draft" | "disabled",
    email: <string>,
    event_ids: [<integer>],
    api_keys: [<string>]
  }
}
```
#### Notes
-  api_keys are required.
- `type`, `l10n`, `start_datetime` and `end_datetime` are required.
- All `l10n` fields, except for `foreword`, are required.
- All `contents` fields are required, except for `thumbnail_url`, which is only required for featured resources (`is_featured: true`).
- When creating a resource of type `gdoa`, if `event_ids` are provided, all `event`s/`external_event`s included will be transformed into `gdoa_event`s/`gdoa_external_event`s and vice-versa.
- When a resource of type `gdoa_event`/`gdoa_external_event` is being created with no `contents.gdoa_id`, it will be transformed into `event`/`external_event`.
- When a resource of type `event`/`external_event` is being created with `contents.gdoa_id`, it will be transformed into `gdoa_event`/`gdoa_external_event`.
- Security level: none. Only **org_admin**, **org_member** or **admin** can send status. Resources not created by **org_admin**, **org_member** or **admin** have their `status`es set to "draft".
- When creating a resource of type `campaign`, embedded `cta`s will be created if they have a key named `params` (note that you don't have to specify `type` in `params` as it will be derived from `type` in its parent). For example:

```json
{
  type: "cta_mass_email",
  title: <string>,
  content: <string>,
  params:
  {
    name: <string>,
    contents:
    {
      to: [<string>]
    }
  }
}
```

## Update resource
#### Request
```json
{
  path: "/api/resource/update/<resource_id>",
  method: "POST",
  body:
  {
    tag_ids: [<integer>],
    l10n: [<l10n>],
    password: <string>,
    contents: <contents>,
    is_featured: <boolean>,
    status: "published" | "draft" | "disabled",
    api_keys: [<string>],
    author_id: <integer>
  }
}
```
#### Notes
- author_id can be send only if user is admin or org_admin
-  api_keys are required.
- All fields are optional. `contents.thumbnail_url` is required for featured resources (`is_featured: true`).
- When updating a resource of type `gdoa`, if `event_ids` are provided, all `event`s/`external_event`s included will be transformed into `gdoa_event`s/`gdoa_external_event`s and vice-versa.
- When a resource of type `gdoa_event`/`gdoa_external_event` is being updated with no `contents.gdoa_id`, it will be transformed into `event`/`external_event`.
- When a resource of type `event`/`external_event` is being updated with `contents.gdoa_id`, it will be transformed into `gdoa_event`/`gdoa_external_event`.
- Security level: **admin**, **org_admin**, **org_member**.
- When updating a resource of type `campaign`, embedded `cta`s will be created if they have a key named `params`. For example:

```json
{
  type: "cta_mass_email",
  title: <string>,
  content: <string>,
  params:
  {
    type: "mass_email",
    name: <string>,
    contents:
    {
      to: [<string>]
    }
  }
}
```

## Put l10n
#### Request
```json
{
  path: "/api/resource/put_l10n/<resource_id>",
  method: "POST",
  body: <l10n>
}
```
#### Notes
- All fields are required.
- Security level: **admin**.

## Mutual response
Success
```json
{
  id: <integer>,
  type: <string>,
  author_id: <integer>,
  status: "published" | "draft" | "disabled",
  l10n: [<l10n>],
  contents: <contents>,
  tag_ids: [<integer>],
  is_featured: <boolean>
  author_id: {Author},
}
|
[...]
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

## TODO
- multiple disabling/reporting/featuring is not implemented yet, use singles for now
- `l10n.contents.endorsements` is not implemented.
- `campaign`
 - `endorsements_count` is not implemented
