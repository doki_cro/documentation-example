## Subscribe
#### Request
```json
{
  path: "/api/tweets",
  method: "POST",
  body:
  {
    hashtags: [<string>]
  }
}
```
#### Response
Success
```json
{
  id: <integer>,
  text: <string>,
  user: <string>
}
```
#### Notes
- Security level: none.
