## Search
#### Request
```json
{
  path: "/api/search/resource",
  method: "POST",
  body:
  {
    tags:
    {
      actor: [<integer>],
      ally: [<integer>],
      country: [<integer>],
      sector: [<integer],
      topic: [<integer>]
    },
    tags: [<integer>],
    exclude_tags:
    {
      actor: [<integer>],
      ally: [<integer>],
      country: [<integer>],
      sector: [<integer],
      topic: [<integer>]
    },
    exclude_tags: [<integer>]
    types: [<resource_type>],
    search_query: <string>,
    offset: <integer>,
    limit: <integer>,
    order_by: "alphabetical" | "inserted" | "updated",
    order_dir: "asc" | "desc",
    lang: <string>,
    status: "published" | "draft" | "disabled",
    start_datetime: <string>,
    end_datetime: <string>,
    is_featured: <boolean>,
    featured_first: <boolean>,
    orgs: [<integer>],
    authors: [<integer>],
    frontend_submissions: <boolean>,
    in_progress: "active" | "finished",
    api_keys: [<string>],
    show_children: <integer>,
    show_parent: <integer>,
    only_root_pages: <integer>
  }
}
```
#### Response
```json
{
  counts: <integer>
  resource: [
  {
    id: <integer>,
    type: <string>,
    author_id: <integer>,
    status: "published" | "draft" | "disabled",
    l10n: [<l10n>],
    contents: <contents>,
    tag_ids: [<integer>],
    inserted_at: <string>,
    updated_at: <string>
  }]
}
```
#### Notes
- Tags can be used with **type** or without it
- All fields are optional except api key.
- `contents.petition_count`, `contents.petition_signature_count` and `contents.subscriber_count` are only returned for `campaign`s.
- `contents.gdoa_events` is only returned for `gdoa`s.
- Security level: none. Only **admin** can filter by `status`.

## Resource counts
#### Request
```json
{
  path: "/api/resource_counts",
  method: "POST",
  body:
  {
    tags:
    {
      actor: [<integer>],
      ally: [<integer>],
      country: [<integer>],
      sector: [<integer],
      topic: [<integer>]
    },
    tags: [<integer>]
    types: [<resource_type>],
    search_query: <string>,
    lang: <string>,
    status: "published" | "draft" | "disabled",
    start_datetime: <string>,
    end_datetime: <string>,
    api_keys: [<string>]
  }
}
```
#### Response
```json
{
  <resource_type>: <integer>
}
```
#### Notes
- Tags can be used with **type** or without it
- All fields are optional except api key.
- Security level: none.

## Resource counts and tags
#### Request
```json
{
  path: "/api/resource_tags_counts",
  method: "POST",
  body:
  {
    tags:
    {
      actor: [<integer>],
      ally: [<integer>],
      country: [<integer>],
      sector: [<integer],
      topic: [<integer>]
    },
    tags: [<integer>]
    types: [<resource_type>],
    search_query: <string>,
    lang: <string>,
    start_datetime: <string>,
    end_datetime: <string>
  }
}
```
#### Response
```json
{
  counts:
  {
    <resource_type>: <integer>
  },
  tags:
  [
    {
      id: <integer>,
      type: "actor" | "ally" | "country" | "sector" | "topic",
      l10n: [<l10n>],
      contents: <contents>,
      status: "published" | "disabled"
    }
  ]
}
```
#### Notes
- Tags can be used with **type** or without it
- All fields are optional.
- Security level: none.

## Stats counts
#### Request
```json
{
  path: "/api/stats_counts",
  method: "POST",
  body:
  {
    tags:
    {
      actor: [<integer>],
      ally: [<integer>],
      country: [<integer>],
      sector: [<integer],
      topic: [<integer>]
    },
    tags: [<integer>]
    types: [<resource_type>],
    start_datetime: <string>,
    end_datetime: <string>
  }
}
- Tags can be used with **type** or without it

```
#### Response
```json
{
  disabled:
  {
    count: <integer>,
    <lang>: <integer>,
    <lang2>: ...
  },
  draft:
  {
    count: <integer>,
    <lang>: <integer>,
    <lang2>: ...
  },
  flagged:
  {
    count: <integer>,
    <lang>: <integer>,
    <lang2>: ...
  },
  published:
  {
    count: <integer>,
    <lang>: <integer>,
    <lang2>: ...
  }
}
```
#### Notes
- All fields are optional.
- Security level: **admin**.

## Tag search
#### Request
```json
{
  path: "/api/search/tag",
  method: "POST",
  body:
  {
    types: [<string>],
    ids: [<integer>],
    search_query: <string>,
    offset: <integer>,
    limit: <integer>,
    order_by: <string>,
    order_dir: <string>,
    lang: <string>,
    status: <string>
  }
```
#### Response
```json
{
  counts: <integer>
  tags: [{
    id: <integer>,
    type: "actor" | "ally" | "country" | "sector" | "topic",
    l10n: [<l10n>],
    contents: <contents>,
    status: "published" | "disabled",
    inserted_at: <string>,
    updated_at: <string>
  }]
}
```
#### Notes
- All fields are optional.
- Security level: none. Only **admin** can filter by `status`.

## Subscriber search
#### Request
```json
  path: "/api/search/subscriber",
  method: "POST",
  body:
  {
    search_query: <string>,
    offset: <integer>,
    limit: <integer>,
    order_by: "alphabetical" | "inserted" | "updated" | "start_datetime" | "end_datetime",
    order_dir: "asc" | "desc",
    lang: <string>,
    country_id: <integer>,
    auto_sign: <boolean>,
    auto_send: <boolean>,
    tags:
    {
      actor: [<integer>],
      ally: [<integer>],
      country: [<integer>],
      sector: [<integer],
      topic: [<integer>]
    },
    tags: [<integer>]
    status: "subscribed" | "unsubscribed",
    campaign_id: <integer>
  }
```
- Tags can be used with **type** or without it

#### Response
```json
[
  {
    id: <integer>,
    name: <string>,
    country_id: <integer>,
    email: <string>,
    organisation: <string>,
    auto_sign: <boolean>,
    auto_send: <boolean>,
    tag_ids: [<integer>],
    status: <string>,
    campaign_ids: [<integer>],
    lang: <string>,
    inserted_at: <string>,
    updated_at: <string>
  }
]
```
#### Notes
- All fields are optional.
- Security level: **org_member**, **org_admin** or **admin**.

## Search
#### Request
```json
{
  path: "/api/search/author",
  method: "POST",
  body:
  {
    tags:
    {
      actor: [<integer>],
      ally: [<integer>],
      country: [<integer>],
      sector: [<integer],
      topic: [<integer>]
    },
    tags: [<integer>],
    search_query: <string>,
    offset: <integer>,
    limit: <integer>,
    order_by: "alphabetical" | "inserted" | "updated",
    order_dir: "asc" | "desc",
    status: "active" | "pending" | "disabled",
    org_id: <integer>,
  }
}
- Tags can be used with **type** or without it

```
#### Response
```json
{
  counts: <integer>
  authors: [
    {
      contents: <map>,
      email:  <string>,
      id: <integer>,
      is_org_admin: <boolean>,
      l10n: <map>,
      last_activity: <string>,
      name: <string>,
      org_id: <integer>,
      status: <string>,
      tag_ids: <list>,
      tags: <list>,
    }
  ]
}
```
#### Notes
- Security level: none. Only **admin** can filter by `status`.
- Security note: **org_od** must be specified if you are not an admin.