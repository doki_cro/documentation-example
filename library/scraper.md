## Scrape Facebook event
#### Request
```json
{
  path: "/api/scraper/fb_event",
  method: "POST",
  body:
  {
    fb_url: <string>
  }
}
```
#### Response
Success
```json
{
  type: "external_event",
  title: <string>,
  description: <string>,
  external_url: <string>,
  source_name: "Facebook",
  thumbnail_url: <string>,
  location: {....},
  start_datetime: <string>,
  end_datetime: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- All fields are required.
- Security level: none.

## Scrape media
#### Request
```json
{
  path: "/api/scraper/media",
  method: "POST",
  body:
  {
    url: <string>
  }
}
```
#### Response
Success
```json
[{
  type: "video" | "audio" | "image",
  title: <string>,
  description: <string>,
  external_url: <string>,
  thumbnail_url: <string>,
  source_name: "Dailymotion" | "Flickr" | "RadioLabour" | "SoundCloud" | "Vimeo" | "Youtube"
}]
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- All fields are required.
- Security level: none.

## Scrape OG tags
#### Request
```json
{
  path: "/api/scraper/og",
  method: "POST",
  body:
  {
    url: <string>
  }
}
```
#### Response
Success
```json
{
  type: <string>,
  title: <string>,
  description: <string>,
  external_url: <string>,
  thumbnail_url: <string>,
  source_name: <string>
}
```
Failure
```json
{
  reason: <string>
}
```
#### Notes
- All fields are required.
- Security level: none.

## Create scraped Facebook page
#### Request
```json
{
  path: "/api/scraper/scraped_fb_page/create",
  method: "POST",
  body:
  {
    fb_url: <string>
  }
}
```
#### Response
Success
```json
{
  page_id: <string>,
  status: "enabled" | "disabled",
  error: <string> | nil,
  inserted_at: <string>,
  updated_at: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- All fields are required.
- Security level: **admin**.

## Delete scraped Facebook page
#### Request
```json
{
  path: "/api/scraper/scraped_fb_page/<page_id>",
  method: "DELETE"
}
```
#### Response
```json
"ok"
```
#### Notes
- Security level: **admin**.

## List scraped Facebook pages
#### Request
```json
{
  path: "/api/scraper/scraped_fb_pages",
  method: "GET"
}
```
#### Response
```json
[
  {
    page_id: <string>,
    status: "enabled" | "disabled",
    error: <string> | nil,
    inserted_at: <string>,
    updated_at: <string>
  }
]
```
#### Notes
- Security level: **admin**.
