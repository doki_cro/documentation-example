## Subscribe
#### Request
```json
{
  path: "/api/subscriber/subscribe",
  method: "POST",
  body:
  {
    name: <string>,
    country_id: <integer>,
    email: <string>,
    organisation: <string>,
    auto_sign: <boolean>,
    auto_send: <boolean>,
    tag_ids: [<integer>],
    campaign_ids: [<integer>],
    lang: <string>,
    website_id: <integer>
  }
}
```
#### Response
Success
```json
{
  id: <integer>,
  name: <string>,
  country_id: <integer>,
  email: <string>,
  organisation: <string>,
  auto_sign: <boolean>,
  auto_send: <boolean>,
  tag_ids: [<integer>],
  status: <string>,
  campaign_ids: [<integer>],
  lang: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- `name`, `email` and `lang`, `website_id`  are required.
- An email is sent to the provided email address containing an edit token. Token is required when updating a subscriber.
- Security level: none.

## Create subscriber
#### Request
```json
{
  path: "/api/subscriber/create",
  method: "POST",
  body:
  {
    name: <string>,
    country_id: <integer>,
    email: <string>,
    organisation: <string>,
    auto_sign: <boolean>,
    auto_send: <boolean>,
    tag_ids: [<integer>],
    campaign_ids: [<integer>],
    lang: <string>,
    website_id: <integer>
  }
}
```
#### Response
Success
```json
{
  id: <integer>,
  name: <string>,
  country_id: <integer>,
  email: <string>,
  organisation: <string>,
  auto_sign: <boolean>,
  auto_send: <boolean>,
  tag_ids: [<integer>],
  status: <string>,
  campaign_ids: [<integer>],
  lang: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- `name`, `email` and `lang`, `website_id`  are required.
- Security level: none.

## Update subscriber
#### Request
```json
{
  path: "/api/subscriber/<subscriber_id>/update",
  method: "POST",
  body:
  {
    name: <string>,
    country_id: <integer>,
    organisation: <string>,
    auto_sign: <boolean>,
    auto_send: <boolean>,
    tag_ids: [<integer>],
    campaign_ids: [<integer>],
    status: <boolean>,
    lang: <string>
  }
}
```
#### Response
Success
```json
{
  id: <integer>,
  name: <string>,
  country_id: <integer>,
  email: <string>,
  organisation: <string>,
  auto_sign: <boolean>,
  auto_send: <boolean>,
  tag_ids: [<integer>],
  status: <string>,
  campaign_ids: [<integer>],
  lang: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- All fields are optional.
- Security level: none.

## Get subscriber
#### Request
```json
{
  path: "/api/subscriber/<subscriber_id>",
  method: "GET"
}
```
#### Response
```json
{
  id: <integer>,
  name: <string>,
  country_id: <integer>,
  email: <string>,
  organisation: <string>,
  auto_sign: <boolean>,
  auto_send: <boolean>,
  tag_ids: [<integer>],
  status: <string>,
  campaign_ids: [<integer>],
  lang: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```
#### Notes
- Security level: none.

## List subscribers
#### Request
```json
{
  path: "/api/list_subscribers",
  method: "GET"
}
```
#### Response
```json
[
  {
    id: <integer>,
    name: <string>,
    country_id: <integer>,
    email: <string>,
    organisation: <string>,
    auto_sign: <boolean>,
    auto_send: <boolean>,
    tag_ids: [<integer>],
    status: <string>,
    campaign_ids: [<integer>],
    lang: <string>,
    inserted_at: <string>,
    updated_at: <string>
  }
]
```
#### Notes
- Security level: **org_member**, **org_admin** or **admin**.

## List campaign susbcribers csv
#### Request
```json
{
  path: "/api/org/<org_id>/campaign/<campaign_id>/subscribers_csv",
  method: "GET"
}
```
#### Response
```
POP <campaign> subscribers.csv
```
#### Notes
- Security level: **org_admin**, **org_member** or **admin**.

## List campaign susbcribers xlsx
#### Request
```json
{
  path: "/api/org/<org_id>/campaign/<campaign_id>/subscribers_xlsx",
  method: "GET"
}
```
#### Response
```
POP <campaign> subscribers.xlsx
```
#### Notes
- Security level: **org_admin**, **org_member** or **admin**.

## List shared susbcribers csv
#### Request
```json
{
  path: "/api/shared_subscribers_csv",
  method: "GET"
}
```
#### Response
```
POP Shared subscribers.csv
```
#### Notes
- Security level: **org_member**, **org_admin** or **admin**.

## List shared susbcribers xlsx
#### Request
```json
{
  path: "/api/shared_subscribers_xlsx",
  method: "GET"
}
```
#### Response
```
POP Shared subscribers.xlsx
```
#### Notes
- Security level: **org_member**, **org_admin** or **admin**.
