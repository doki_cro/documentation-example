## Get reports
#### Request
```json
{
  path: "/api/resource/<resource_id>/reports",
  method: "GET"
}
```
#### Notes
- Security level: **admin**.

## Create report
#### Request
```json
{
  path: "/api/report/create",
  method: "POST",
  body:
  {
    resource_id: <integer>,
    reason: "wrong_content" | "spam" | "defamatory" | "wrong_content",
    description: <string>,
    email: <string>
  }
}
```
#### Notes
- `resource_id` and `reason` are required.
- Security level: none.

## Update report
#### Request
```json
{
  path: "/api/report/update/<report_id>",
  method: "POST",
  body:
  {
    is_approved: <boolean> | null
  }
}
```
#### Notes
- All fields are optional.
- Security level: **admin**.

## Mutual response
Success
```json
{
  id: <integer>,
  resource_id: <integer>,
  lang: <string>,
  reason: "wrong_content" | "spam" | "defamatory" | "wrong_content",
  description: <string>,
  email: <string>,
  is_approved: <boolean>,
  inserted_at: <string>,
  updated_at: <string>
}
|
[...]
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
