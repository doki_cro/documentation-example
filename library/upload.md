## Upload file
#### Request
```json
{
  path: "/api/upload",
  method: "POST",
  body:
  {
    file: <file>
  }
}
```
#### Response
Success
```json
{
  url: <string>,
  title: <string>,
  type: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
