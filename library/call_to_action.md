## Base
```json
{
  id: <integer>,
  campaign_id: <integer>,
  name: <string>,
  type: "cta_petition" | "cta_tweet" | "cta_retweet" | "cta_act" | "cta_mass_email" | "cta_event",
  contents: <contents>,
  inserted_at: <string>,
  updated_at: <string>
}
```

## `contents`
#### `cta_petition` | `cta_tweet` | `cta_act` | `cta_external_act`
```json
{}
```
#### `cta_retweet`
```json
{
  cta_tweet: <string>
}
```
#### `cta_mass_email`
```json
{
  title: <string>,
  content: <string>,
  to: [<string>]
}
```
#### `cta_event`
```json
{
  cta_event_id: <integer>
}
```

## Get cta_petition
#### Request
```json
{
  path: "/api/cta_petition/<cta_petition_id>",
  method: "GET",
}
```
#### Response
Success
```json
{
  id: <integer>,
  campaign_id: <integer>,
  type: <string>,
  name: <string>,
  contents: <contents>,
  inserted_at: <string>,
  updated_at: <string>
}
```
#### Notes
- `contents` contains `signatures` (total count of signed cta_petitions).
- `contents` contains `goal` (goal of the cta_petition).

## Sign cta_petition
#### Request
```json
{
  path: "/api/cta_petition/<cta_petition_id>/sign",
  method: "POST",
  body:
  {
    name: <string>,
    country_id: <integer>,
    email: <string>,
    organisation: <string>,
    auto_sign: <boolean>,
    is_subscribed: <boolean>,
    lang: <string>,
    website_id: <integer>,
  }
}
```
#### Response
Success
```json
{
  cta_petition_id: <integer>,
  subscriber_id: <integer>,
  is_auto_signed: <boolean>,
  name: <string>,
  country_id: <integer>,
  email: <string>,
  organisation: <string>,
  lang: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- `email` and  `website_id` is required.
- Creates a subscriber if one does not exist. Performs this check with `email`.
- Security level: none.

## Unsign cta_petition
#### Request
```json
{
  path: "/api/cta_petition/<cta_petition_id>/unsign",
  method: "POST",
  body:
  {
    subscriber_id: <integer>
  }
}
```
#### Response
```json
"ok"
```
#### Notes
- All fields are required.
- Security level: none.

## Get cta_petition signatures
#### Request
```json
{
  path: "/api/org/<org_id>/cta_petition/<cta_petition_id>/signatures",
  method: "GET"
}
```
#### Response
```json
[
  {
    cta_petition_id: <integer>,
    subscriber_id: <integer>,
    is_auto_signed: <boolean>,
    inserted_at: <string>,
    updated_at: <string>
  }
]
```
#### Notes
- Security level: **org_admin**, **org_member** or **admin**.

## List cta_petition signatures csv
#### Request
```json
{
  path: "/api/org/<org_id>/cta_petition/<cta_petition_id>/signatures_csv",
  method: "GET"
}
```
#### Response
```
POP <cta_petition name> signatures.csv
```
#### Notes
- Security level: **org_admin**, **org_member** or **admin**.

## List cta_petition signatures xlsx
#### Request
```json
{
  path: "/api/org/<org_id>/cta_petition/<cta_petition_id>/signatures_xlsx",
  method: "GET"
}
```
#### Response
```
POP <cta_petition name> signatures.xlsx
```
#### Notes
- Security level: **org_admin**, **org_member** or **admin**.

## Send email
#### Request
```json
{
  path: "/api/send_email/:cta_mass_email_id/send",
  method: "POST",
  body:
  {
    name: <string>,
    country_id: <integer>,
    email: <string>,
    organisation: <string>,
    auto_sign: <boolean>,
    is_subscribed: <boolean>,
    title: <string>,
    content: <string>,
    lang: <string>
  }
}
```
#### Response
Success
```json
{
  cta_mass_email_id: <integer>,
  subscriber_id: <integer>,
  is_auto_sent: <boolean>,
  name: <string>,
  country_id: <integer>,
  email: <string>,
  organisation: <string>,
  title: <string>,
  content: <string>,
  lang: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- `email` is required.
- Creates a subscriber if one does not exist. Performs this check with `email`.
- Security level: none.
