## Create newsletter
#### Request
```json
{
  path: "/api/newsletter/create",
  method: "POST",
  body:
  {
    id: <integer>,
    campaign_id: <integer>,
    name: <string>,
    l10n: [{
      title: <string>,
      content: <string>
    }],
    status: "send" | "draft",
    from: <string>,
    from_name: <string>,
    send_time: <string>,
    url: <string>,
    track_opens: <boolean>
  }
}
```
#### Response
Success
```json
{
    id: <integer>,
    campaign_id: <integer>,
    name: <string>,
    l10n: [{
      title: <string>,
      content: <string>
    }],
    status: "send" | "draft",
    from: <string>,
    from_name: <string>,
    send_time: <string>,
    url: <string>,
    open_count: <integer>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- Security level: **admin**.
- `campaign_id` is optional. If empty email will be sent to every subscriber.

## Update newsletter
#### Request
```json
{
  path: "/api/newsletter/<newsletter_id>/update",
  method: "POST",
  body:
  {
    id: <integer>,
    campaign_id: <integer>,
    name: <string>,
    l10n: [{
      title: <string>,
      content: <string>
    }],
    status: "send" | "draft",
    from: <string>,
    from_name: <string>,
    send_time: <string>,
    url: <string>,
    track_opens: <boolean>
  }
}
```
#### Response
Success
```json
{
    id: <integer>,
    campaign_id: <integer>,
    name: <string>,
    l10n: [{
      title: <string>,
      content: <string>
    }],
    status: "send" | "draft",
    from: <string>,
    from_name: <string>,
    send_time: <string>,
    url: <string>,
    open_count: <integer>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- All fields are optional.
- Security level: **admin**.

## Get newsletter
#### Request
```json
{
  path: "/api/newsletter/<newsletter_id>",
  method: "GET"
}
```
#### Response
```json
{
    id: <integer>,
    campaign_id: <integer>,
    name: <string>,
    l10n: [{
      title: <string>,
      content: <string>
    }],
    status: "send" | "draft",
    from: <string>,
    from_name: <string>,
    send_time: <string>,
    url: <string>,
    open_count: <integer>
}
```
#### Notes
- Security level: **admin**.

## List susbcribers
#### Request
```json
{
  path: "/api/newsletters",
  method: "GET"
}
```
#### Response
```json
[
  {
    id: <integer>,
    campaign_id: <integer>,
    name: <string>,
    country_id: <integer>,
    email: <string>,
    auto_sign: <boolean>,
    tag_ids: [<integer>],
    status: <string>,
    lang: <string>,
    inserted_at: <string>,
    updated_at: <string>
  }
]
```
#### Notes
- Security level: **admin**.
