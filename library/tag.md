## l10n
#### "type" | "actor" | "ally"
```json
{
  lang: <string>,
  name: <string>,
  abbreviation: <string>,
  summary: <string>
}
```
#### "country" | "sector" | "topic"
```json
{
  lang: <string>,
  name: <string>,
  abbreviation: <string>
}
```

## Contents
#### "type" | "actor" | "ally"
```json
{
  external_url: <string>,
  thumbnail_url: <string>
}
```
#### "country" | "sector" | "topic"
```
{}
```

## Create tag
#### Request
```json
{
  path: "/api/tag/create",
  method: "POST",
  body:
  {
    type: "type" | "actor" | "ally" | "country" | "sector" | "topic",
    l10n: [<l10n>],
    contents: <contents>,
    tag_ids: [<integer>]
  }
}
```
#### Notes
- All fields are required except tag_ids. Created tag's status will default to "published".
- Security level: **logged in user**.

## Update tag
#### Request
```json
{
  path: "/api/tag/update/<tag_id>",
  method: "POST",
  body:
  {
    type: <string>,
    l10n: [<l10n>],
    contents: <contents>,
    status: "published" | "disabled",
    tag_ids: [<integer>]
  }
}
```
#### Notes
- All fields are optional.
- Security level: **admin**.

## List tags
#### Request
```json
{
  path: "/api/list_tags",
  method: "GET",
  body:
  {
    sort_lang: "String",
  }
}
```
#### Notes
By default list_tags is sorted by en if you want you can specify lang by using list_tags?sort_lang=fr after fr lang all other languages will be displayed.
Security level: none. Only **admin** can see "disabled" tags.

## Get tag
#### Request
```json
{
  path: "/api/tag/<tag_id>",
  method: "GET"
}
```
#### Notes
Security level: none. Only **admin** can see "disabled" tags.

## Response
Success
```json
{
  id: <integer>,
  type: "type" | "actor" | "ally" | "country" | "sector" | "topic",
  l10n: [<l10n>],
  contents: <contents>,
  status: "published" | "disabled",
  resources_count: <integer>
  inserted_at: <string>,
  updated_at: <string>,
  tag_ids: [<integer>]
}
|
[...]
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

## Delete tag
#### Request
```json
{
  path: "/api/tag/<tag_id>",
  method: "DELETE"
```

#### Notes
Security level: **admin**

## Merge tags
#### Request
```json
{
  path: "/api/tags/merge",
  method: "POST",
  body:
  {
    merge_to_id: <integer>,
    merge_tags_ids: [<integer>]
  }
```

#### Notes
Security level: **admin**