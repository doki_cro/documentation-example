## Get petition signatures
#### Request
```json
{
  path: "/api/org/<org_id>/petition/<petition_id>/signatures",
  method: "GET"
}
```
#### Response
```json
[
  {
    petition_id: <integer>,
    subscriber_id: <integer>,
    is_auto_signed: <boolean>,
    inserted_at: <string>,
    updated_at: <string>,
    subscriber: [
      tags_ids: <array>,
      status: <string>,
      organisation: <string>,
      name: <string>,
      lang: <string>,
      id: <integer>,
      email: <string>,
      country_id: <integer>,
      campaign_ids: <array>,
      auto_sign: <bool>,
      auto_send: <bool>,
    ],
    country: [
      type: <string>,
      l10n: <array>,
      id: <integer>,
      contents: <array>,
    ]
  }
]
```
#### Notes
- Security level: **org_admin**, **org_member** or **admin**.