## Contents
#### `contribution`
```json
{
  resource_id: <integer>
}
```
#### `report`
```json
{
  resource_id: <integer>,
  report_id: <integer>
}
```

## List notifications
#### Request
```json
{
  path: "/api/list_notifications",
  method: "POST",
  body:
  {
    offset: <integer>,
    limit: <integer>
  }
}
```
#### Response
```json
[
  {
    id: <integer>,
    type: "contribution" | "report",
    contents: <contents>,
    is_read: <boolean>
  }
]
```
#### Notes
- If not provided, `offset` and `limit` will default to `0` and `20`, respectively.
- Security level: **admin**.

## Read notification
#### Request
```json
{
  path: "/api/notification/read/<notification_id>,
  method: "POST",
  body: {}
}
```
#### Response
```json
"ok"
```
#### Notes
- Security level: **admin**.

## Notes
The following actions will create notifications for all system admins:
- Creating a resource with `status: "draft"` (`type: "contribution"`)
- Reporting a resource (`type: "report"`)
