The following actions are permitted per user role:  
- Organization member
 - View petition signatures
 - Create resource as non-draft
 - View campaign subscribers
 - View global subscribers
 - Search subscribers

- Organization administrator
 - All org-member permissions
 - View own organization
 - Update own organization (except for `status`)
 - Invite user (existing or new) into own organization
 - View/update own organization's members

- System administrator
 - All org-admin permissions
 - View reports per resource
 - Update report
 - View non-`published` resource (its reports as well)
 - Update resource
 - Add internationalized content to a resource
 - Search resources by `status`
 - Statistic counts (per status)
 - Search tags by `status`
 - View/create/delete scraped Facebook page
 - Create/update tag
 - View `disabled` tags
 - Create/update/delete internationalized user interface texts
 - View organizations
 - Update organization (its `status` as well)
 - View/create/update newsletters
 - List notifications and mark them as read

| Action                                                      | OrgMember          | OrgAdmin           | SystemAdmin        |
| ----------------------------------------------------------- | :----------------: | :----------------: | :----------------: |
| View petition signatures                                    | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| Create resource as non-draft                                | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| View subscribers per campaign                               | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| View global subscribers                                     | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| Search subscribers                                          | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| View own organization                                       |                    | :heavy_check_mark: | :heavy_check_mark: |
| Update own organization (except for `status`)               |                    | :heavy_check_mark: | :heavy_check_mark: |
| Invite user (existing or new) into own organization         |                    | :heavy_check_mark: | :heavy_check_mark: |
| View/update own organization's members                      |                    | :heavy_check_mark: | :heavy_check_mark: |
| View reports per resource                                   |                    |                    | :heavy_check_mark: |
| Update report                                               |                    |                    | :heavy_check_mark: |
| View non-`published` resource (its reports as well)         |                    |                    | :heavy_check_mark: |
| Update resource                                             |                    |                    | :heavy_check_mark: |
| Add internationalized content to a resource                 |                    |                    | :heavy_check_mark: |
| Search resources by `status`                                |                    |                    | :heavy_check_mark: |
| Statistic counts (per status)                               |                    |                    | :heavy_check_mark: |
| Search tags by `status`                                     |                    |                    | :heavy_check_mark: |
| View/create/delete scraped Facebook page                    |                    |                    | :heavy_check_mark: |
| Create/update tag                                           |                    |                    | :heavy_check_mark: |
| View `disabled` tags                                        |                    |                    | :heavy_check_mark: |
| Create/update/delete internationalized user interface texts |                    |                    | :heavy_check_mark: |
| View organizations                                          |                    |                    | :heavy_check_mark: |
| Update organization (its `status` as well)                  |                    |                    | :heavy_check_mark: |
| View/create/update newsletters                              |                    |                    | :heavy_check_mark: |
| List notifications and mark them as read                    |                    |                    | :heavy_check_mark: |
