## Contents
[Same as contents of map with key `en`](../apps/pop/lib/pop/client_l10n_data.ex)

#### Notes
- All fields are optional.

## List client l10ns
#### Request
```json
{
  path: "/api/list_client_l10ns",
  method: "GET"
}
```
#### Response
```json
[
  {
    lang: <string>,
    contents: <contents>
  }
]
```

## Create client l10n
#### Request
```json
{
  path: "/api/client_l10n/create",
  method: "POST",
  body:
  {
    lang: <string>,
    contents: <contents>
  }
}
```
#### Response
Success
```json
{
  lang: <string>,
  contents: <contents>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- All fields are required.
- Security level: **admin**.

## Update client l10n
#### Request
```json
{
  path: "/api/client_l10n/<lang>/update",
  method: "POST",
  body:
  {
    contents: <contents>
  }
}
```
#### Response
Success
```json
{
  lang: <string>,
  contents: <contents>
}
```
Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
#### Notes
- All fields are optional.
- Security level: **admin**.

## Delete client l10n
#### Request
```json
{
  path: "/api/client_l10n/<lang>",
  method: "DELETE"
}
```
#### Response
```json
"ok"
```
#### Notes
- Security level: **admin**.
