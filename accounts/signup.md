## Signup and create new organisation
#### Request
```json
{
  path: "/api/signup",
  method: "POST",
  body:
  {
    user:
    {
      email: <string>,
      password: <string>,
      name: <string>
    },
    org:
    {
      full_name: <string>,
      abbreviation: <string>,
      address: <string>,
      country_id: <integer>,
      email: <string>,
      phone_number: <string>,
      logo_url: <string>,
      description: <string>,
      source:
      {
        facebook: <string>,
        twitter: <string>
      },
      facts:
      {
        <key>: <value>
      }
    },
    api_keys: [<string>]
  }
}
```

#### Response
Success
```json
{
  user:
  {
    id: <integer>,
    email: <string>,
    name: <string>,
    is_admin: <boolean>,
    is_org_admin: <boolean>,
    last_activity: <string>,
    inserted_at: <string>,
    updated_at: <string>
  },
  org:
  {
    id: <integer>,
    full_name: <string>,
    abbreviation: <string>,
    address: <string>,
    country_id: <integer>,
    email: <string>,
    phone_number: <string>,
    logo_url: <string>,
    description: <string>,
    source:
    {
      facebook: <string>,
      twitter: <string>
    },
    facts:
    {
      <key>: <value>
    },
    status: "active" | "pending" | "disabled",
    inserted_at: <string>,
    updated_at: <string>
  }
}
```

Failure
```json
{
  org:
  [{
    name: <field_name>,
    message: <error_code>,
    opts: <opts_map>,
    l10n: [{
      lang: <language_code>,
      message: <error_string>
    }]
  }],
  user:
  [{
    name: <field_name>,
    message: <error_code>,
    opts: <opts_map>,
    l10n: [{
      lang: <language_code>,
      message: <error_string>
    }]
  }]
}
```

#### Notes
- Api keys are required
- Org: `full_name` and `country_id` are required.
- User: all fields are required.
- Org's `email` defaults to user's, if it wasn't given one.

## Signup to an existing organisation
#### Request
```json
{
  path: "/api/signup",
  method: "POST",
  body:
  {
    user: {
      email: <string>,
      password: <string>,
      name: <string>,
      org_id: <integer>
    },
    api_keys: [<string>]
  }
}
```
#### Notes
- Api keys are required

#### Response
Success
```json
{
  user:
  {
    id: <integer>,
    email: <string>,
    name: <string>,
    is_admin: <boolean>,
    is_org_admin: <boolean>,
    last_activity: <string>,
    inserted_at: <string>,
    updated_at: <string>
  }
}
```

## Signup on org invite
#### Request
```json
{
  path: "/api/signup_org_inv",
  method: "POST",
  body:
  {
    token: <string>,
    email: <string>,
    password: <string>,
    name: <string>
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  is_admin: <boolean>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- All fields are required.
- User will get `token` by landing on `http://<host>/admin/signup/<token>`. This URL is provided to user via email given to "Org invite".
