## Update lang
#### Request
```json
{
  lang: <string>
}
```

#### Response
```json
{
  lang: <string>
}
```

#### Notes
- Puts `lang` into session cookie.
