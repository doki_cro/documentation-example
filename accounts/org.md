## Org invite
#### Request
```json
{
  path: "/api/org/<org_id>/invite",
  method: "POST",
  body:
  {
    email: <string>,
    name: <string>,
    is_org_admin: <boolean>
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  org_id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      role: <string>,
      description: <string>,
      phone: <string>
    }
  ],
  contents: {
    thumbnail_urls: <array:string>
  },
  is_org_admin: <boolean>,
  inserted_at: <string>,
  updated_at: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- All fields are required.
- An invitation email is sent to the provided email address, with a link to the signup URL, i.e. `http://<host>/admin/signup/<token>`, which should use "Signup on org invite".
- Security level: **admin** or **org_admin**.

## Get org
#### Request
```json
{
  path: "/api/org/<org_id>",
  method: "GET"
```

#### Response
Success
```json
{
  id: <integer>,
  full_name: <string>,
  abbreviation: <string>,
  address: <string>,
  country_id: <integer>,
  region_id: <integer>,
  l10n: [<l10n>],
  contents: <contents>,
  affiliate_of_id: <integer>
  email: <string>,
  phone_numbers: [{type: "tel"|"fax", number: <string>}],
  logo_url: <string>,
  description: <string>,
  websites: [<websites>]
  source:
  {
    facebook: <string>,
    twitter: <string>
  },
  facts:
  {
    <key>: <value>
  },
  status: "active" | "pending" | "disabled",
  inserted_at: <string>,
  updated_at: <string>
}
```

#### Notes
- Security level: **admin** or **org_admin**.

## Update org
#### Request
```json
{
  path: "/api/org/<org_id>/update",
  method: "POST",
  body:
  {
    full_name: <string>,
    abbreviation: <string>,
    address: <string>,
    country_id: <integer>,
    region_id: <integer>,
    l10n: [<l10n>],
    contents: <contents>,
    affiliate_of_id: <integer>
    email: <string>,
    phone_numbers: [{type: "tel"|"fax", number: <string>}],
    logo_url: <string>,
    description: <string>,
    source:
    {
      facebook: <string>,
      twitter: <string>
    },
    facts:
    {
      <key>: <value>
    },
    status: "active" | "pending" | "disabled",
    api_keys: [<string>]
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  full_name: <string>,
  abbreviation: <string>,
  address: <string>,
  country_id: <integer>,
  region_id: <integer>,
  l10n: [<l10n>],
  contents: <contents>,
  affiliate_of_id: <integer>,
  email: <string>,
  phone_numbers: [{type: "tel"|"fax", number: <string>}],
  logo_url: <string>,
  description: <string>,
  source:
  {
    facebook: <string>,
    twitter: <string>
  },
  facts:
  {
    <key>: <value>
  },
  status: "active" | "pending" | "disabled",
  inserted_at: <string>,
  updated_at: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```
### Notes
- Only **admin** can update websites and affiliate_of_id

## Request deletion of org
#### Request
```json
{
  path: "/api/org/<org_id>/request_deletion",
  method: "POST"
```

#### Notes
- Security level: **org_admin**

## Delete org
#### Request
```json
{
  path: "/api/org/<org_id>",
  method: "DELETE"
```

#### Notes
- Security level: **admin**


#### Notes
- All fields are optional.
- Security level: **admin** or **org_admin**. Only **admin** can change an org's `status`.

## List orgs
#### Request
```json
{
  path: "/api/list_orgs",
  method: "GET",
  body: {
    api_keys: [<string>]
  }
}
```

#### Response admin
```json
[
  {
    id: <integer>,
    full_name: <string>,
    abbreviation: <string>,
    address: <string>,
    country_id: <integer>,
    region_id: <integer>,
    l10n: [<l10n>],
  contents: <contents>,
  affiliate_of_id: <integer>
    email: <string>,
    phone_numbers: [{type: "tel"|"fax", number: <string>}],
    logo_url: <string>,
    description: <string>,
    source:
    {
      facebook: <string>,
      twitter: <string>
    },
    facts:
    {
      <key>: <value>
    },
    status: "active" | "pending" | "disabled",
    inserted_at: <string>,
    updated_at: <string>,
    statistics: {
      user_count: <integer>,
      resource_counts: {<resource_type>: <integer>},
      last_activity: <string>
    }
  }
]
```

#### Response guest
```json
[
  {
    id: <integer>,
    full_name: <string>,
  }
]
```
#### Notes
- Api keys are required

## Get org member
#### Request
```json
{
  path: "/api/org/<org_id>/member/<user_id>",
  method: "GET"
}
```

#### Response
Success
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      role: <string>,
      description: <string>,
      phone: <string>
    }
  ],
  contents: {
    thumbnail_urls: <array:string>
  },
  org_id: <integer>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

#### Notes
- Security level: **admin** or **org_admin**.

## Create org member
#### Request
```json
{
  path: "/api/org/<org_id>/member",
  method: "POST",
  body:
  {
    is_org_admin: <boolean>,
    status: "active" | "pending",
    l10n: [
    {
        lang: <string>,
        role: <string>,
        description: <string>,
        phone: <string>
      }
    ],
    contents: {
      thumbnail_urls: <array:string>
    },
    email: <string>,
    password: <string>,
    name: <string>
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      role: <string>,
      description: <string>,
      phone: <string>
    }
  ],
  contents: {
    thumbnail_urls: <array:string>
  },
  org_id: <integer>,
  status: <string>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- Security level: **admin** or **org_admin**.

## Update org member
#### Request
```json
{
  path: "/api/org/<org_id>/member/update/<user_id>",
  method: "POST",
  body:
  {
    is_org_admin: <boolean>,
    status: "active" | "pending",
    l10n: [
    {
        lang: <string>,
        role: <string>,
        description: <string>,
        phone: <string>
      }
    ],
    contents: {
      thumbnail_urls: <array:string>
    },
    email: <string>,
    password: <string>,
    name: <string>
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      role: <string>,
      description: <string>,
      phone: <string>
    }
  ],
  contents: {
    thumbnail_urls: <array:string>
  },
  org_id: <integer>,
  status: <string>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- Security level: **admin** or **org_admin**.
- Admin can send additional **organisation_id** field that will update organisation

## Delete org member
#### Request
```json
{
  path: "/api/org/<org_id>/member/<user_id>",
  method: "DELETE"
}
```

#### Notes
- Security level: **admin** or **org_admin**.

## List org members
#### Request
```json
{
  path: "/api/org/<org_id>/members",
  method: "GET",
  body:
  {
    status: "active" | "pending"
  }
}
```

#### Response
Success
```json
[
  {
    id: <string>,
    email: <string>,
    name: <string>,
    l10n: [
      {
        lang: <string>,
        role: <string>,
        description: <string>,
        phone: <string>
      }
    ],
    contents: {
      thumbnail_urls: <array:string>
    },
    status: <string>,
    org_id: <integer>,
    is_org_admin: <boolean>,
    last_activity: <string>,
    inserted_at: <string>,
    updated_at: <string>
  }
]
```

#### Notes
- Security level: **admin** or **org_admin**.
- Status defaults to **active**

## Endorse
#### Request
```json
{
  path: "/api/org/<org_id>/endorse/<resource_id>",
  method: "POST",
  body: {
    api_keys: [<string>]
  }
}
```

#### Notes
- api_keys are required.
- Security level: **org_admin**.

## Un endorse
#### Request
```json
{
  path: "/api/org/<org_id>/endorse/<resource_id>",
  method: "DELETE",
  body: {
    api_keys: [<string>]
  }
}
```

#### Notes
- api_keys are required.
- Security level: **org_admin**.