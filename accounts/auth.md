## Login
#### Request
```json
{
  path: "/api/login",
  method: "POST",
  body:
  {
    email: <string>,
    password: <string>
  }
}
```

#### Response
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      foreword: <string>,
      description: <string>,
      contents: <array:map>
    }
  ],
  contents: {
    thumbnail_urls: <map>,
    additionals: <map>
  },
  org_id: <integer>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

#### Notes
- Logged-in users' data is stored in "user" cookie, as a base 64-encoded JSON map. It contains the same data as login response.
- Logins:
 - Admin: `{email: "admin@peopleoverprof.it", password: "testtest"}`
 - Org admin: `{email: "org_admin@peopleoverprof.it", password: "testtest"}`
 - Org member: `{email: "org_member@peopleoverprof.it", password: "testtest"}`

## Logout
#### Request
```json
{
  path: "/api/logout",
  method: "POST",
  body: {}
}
```

#### Response
```json
"ok"
```
