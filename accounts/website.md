## Create new websites
#### Request
```json
{
  path: "/api/org/:id/websites",
  method: "POST",
  body:
  {
    permissions: <array:string>,
    org_id: <integer>,
    url: <string>
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  org_id: <integer>,
  api_key: <string>
  permissions: <array:string>,
  url: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- Security level: **admin**

## List websites
#### Request
```json
{
  path: "/api/websites",
  method: "GET",
  body:
  {
    org_ids: <array:integer>,
  }
}
```

#### Response
Success
```json
[{
  id: <integer>,
  org_id: <integer>,
  api_key: <string>
  permissions: <array:string>,
  url: <string>
}]
```

#### Notes
- Security level: **admin**

## Get website
#### Request
```json
{
  path: "/api/org/:id/websites/:id",
  method: "GET"
}
```

#### Response
Success
```json
{
  id: <integer>,
  org_id: <integer>,
  api_key: <string>
  permissions: <array:string>,
  url: <string>
}
```

#### Notes
- Security level: **admin**

## Update website
#### Request
```json
{
  path: "/api/org/:id/websites/:id",
  method: "PUT",
  body: {
    permissions: <array:string>,
    org_id: <integer>,
    url: <string>
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  org_id: <integer>,
  api_key: <string>
  permissions: <array:string>,
  url: <string>
}
```

#### Notes
- Security level: **admin**

## Delete website
#### Request
```json
{
  path: "/api/org/:id/websites/:id",
  method: "DELETE"
}
```

#### Notes
- Security level: **admin**