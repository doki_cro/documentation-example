## Get author
#### Request
```json
{
  path: "/api/author/<author_id>",
  method: "GET"
}
```

#### Response
Success
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      role: <string>,
      description: <string>,
      foreword: <string>,
      phone: <string>
    }
  ],
  contents: {
    thumbnail_urls: <array:string>
  },
  org_id: <integer>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

## Get user info
#### Request
```json
{
  path: "/api/user/my_accounts",
  method: "GET"
}
```
#### Response
{
  id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      foreword: <string>,
      description: <string>,
      contents: <array:map>,
      position:  <string>
    }
  ],
  contents: {
    thumbnail_urls: <map>,
    additionals: <map>
  },
  org_id: <integer>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}

## Update password
#### Request
```json
{
  path: "/api/user/update_password",
  method: "POST",
  body:
  {
    password: <string>,
    old_password: <string>
  }
}
```

#### Response
Success
```json
"ok"
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- All fields are required.
- Security level: **logged_in**.

## Update name
#### Request
```json
{
  path: "/api/user/update",
  method: "POST",
  body:
  {
    name: <string>,
    l10n: [
      {
        lang: <string>,
        foreword: <string>,
        description: <string>,
        contents: <array:map>,
        position:  <string>
      }
    ],
    contents: {
      thumbnail_urls: <map>,
      additionals: <map>
    }
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  l10n: [
    {
      lang: <string>,
      foreword: <string>,
      description: <string>,
      contents: <array:map>,
      position:  <string>
    }
  ],
  contents: {
    thumbnail_urls: <map>,
    additionals: <map>
  },
  org_id: <integer>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- Security level: **logged_in**.

## Request password reset
#### Request
```json
{
  path: "/api/request_password_reset",
  method: "POST",
  body:
  {
    email: <string>
  }
}
```

#### Response
Success
```json
"ok"
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- An email is sent with reset link.
- Security level: none.

## Reset password
#### Request
```json
{
  path: "/api/reset_password/<token>",
  method: "POST",
  body:
  {
    password: <string>
  }
}
```

#### Response
Success
```json
"ok"
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- Security level: none.

## Request email change
#### Request
```json
{
  path: "/api/request_email_change",
  method: "POST",
  body:
  {
    email: <string>
  }
}
```

#### Response
Success
```json
"ok"
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- An email is sent with change link.
- Security level: none.

## Change email
#### Request
```json
{
  path: "/api/change_email/<token>",
  method: "POST",
  body:
  {
    email: <string>
  }
}
```

#### Response
Success
```json
{
  id: <integer>,
  email: <string>,
  name: <string>,
  org_id: <integer>,
  is_org_admin: <boolean>,
  last_activity: <string>,
  inserted_at: <string>,
  updated_at: <string>
}
```

Failure
```json
[{
  name: <field_name>,
  message: <error_code>,
  opts: <opts_map>,
  l10n: [{
    lang: <language_code>,
    message: <error_string>
  }]
}]
```

#### Notes
- Security level: none.
